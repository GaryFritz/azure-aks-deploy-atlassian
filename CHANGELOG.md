# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-aks-deploy for future maintenance purposes.
